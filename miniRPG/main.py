# -*- coding:UTF-8 -*-
from miniRPG.model import *

hero = Hero("Arthur", 100, 2, "Excalibur")
hero.leave = lambda : print("The nest is clear.")

nest = Nest(
    Monster("Slime", 10, 1, "body"),
    Monster("Goblin", 15, 2, "club"),
    Monster("Death Knight", 20, 3, Weapon("lance", 3)),
    Monster("Cyclops", 30, 5)   #use default weapon
)

hero.burstInto(nest)
if(hero.isAlive()):
    hero.leave()

print(hero._Character__name + ":{0}".format(hero._hp))                 #don't do this