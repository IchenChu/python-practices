class Match(object):
    @staticmethod
    def begin(hero, monster):
        print(monster.getName() + " showed up!")
        while(hero.isAlive()):
            hero.attack(monster)
            if(monster.isAlive()):
                monster.attack(hero)
            else:
                break
        print("Match is over.")
        print("====")